package ru.itis.formulas.function

import ru.itis.formulas.model.Planet
import ru.itis.formulas.utils.Constant

/**
 * Вычисление кинетической энергии тела на планете X
 * @param mass масса тела
 * @param velocity скорость тела
 * @return кинетическая энергия тела на планете X
 */
fun calculateKineticEnergy(planet: Planet, mass: Double, velocity: Double): Double {
    return 0.5 * mass * velocity * velocity
}

/**
 * Вычисление потенциальной энергии тела на планете X
 * @param mass масса тела
 * @param height высота над поверхностью планеты X
 * @return потенциальная энергия тела на планете X
 */
fun calculatePotentialEnergy(planet: Planet, mass: Double, height: Double): Double {
    return mass * planet.gravitationalAcceleration * height
}

/**
 * Вычисление полной механической энергии тела на планете X
 * @param kineticEnergy кинетическая энергия тела
 * @param potentialEnergy потенциальная энергия тела
 * @return полная механическая энергия тела на планете X
 */
fun calculateTotalMechanicalEnergy(kineticEnergy: Double, potentialEnergy: Double): Double {
    return kineticEnergy + potentialEnergy
}

/**
 * Вычисление энергии Гравитации между двумя телами на планете X
 * @param mass1 масса первого тела
 * @param mass2 масса второго тела
 * @param distance расстояние между телами
 * @return энергия Гравитации между двумя телами на планете X
 */
fun calculateGravitationalEnergy(planet: Planet, mass1: Double, mass2: Double, distance: Double): Double {
    return -Constant.GRAVITATIONAL_CONSTANT * mass1 * mass2 / distance
}
