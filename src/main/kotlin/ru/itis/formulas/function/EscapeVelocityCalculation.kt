package ru.itis.formulas.function

import ru.itis.formulas.model.Planet
import ru.itis.formulas.utils.Constant
import kotlin.math.sqrt

/**
 * Calculates the escape velocity for a given planet.
 * @param planet The planet for which the escape velocity is calculated.
 * @return The escape velocity in meters per second.
 */
fun Planet.calculateEscapeVelocity(): Double {
    return sqrt(2 * Constant.GRAVITATIONAL_CONSTANT * this.mass / this.radius)
}


