package ru.itis.formulas.gui

import ru.itis.formulas.function.*
import ru.itis.formulas.utils.*

import javafx.application.Application
import javafx.geometry.Insets
import javafx.geometry.Pos
import javafx.scene.Scene
import javafx.scene.control.*
import javafx.scene.layout.GridPane
import javafx.scene.layout.VBox
import javafx.stage.Stage

class WeightCalculatorGUI : Application() {
    private lateinit var planetComboBox: ComboBox<String>
    private lateinit var massTextField: TextField
    private lateinit var weightLabel: Label

    private val weightCalculation = WeightCalculation()

    override fun start(primaryStage: Stage) {
        primaryStage.title = "Weight Calculator"

        val tabPane = TabPane()
        val planetsTab = Tab("Planets").apply {
            // Создаем GridPane для размещения информации о планетах
            val planetsGrid = GridPane().apply {
                alignment = Pos.CENTER
                hgap = 10.0
                vgap = 10.0
                padding = Insets(25.0, 25.0, 25.0, 25.0)
            }

            // Добавляем информацию о планетах на GridPane
            planetsGrid.add(Label("Planet"), 0, 0)
            planetsGrid.add(Label("Gravity (m/s^2)"), 1, 0)
            planetsGrid.add(Label("Radius (km)"), 2, 0)
            planetsGrid.add(Label("Mass (kg)"), 3, 0)
            planetsGrid.add(Label("First Space Velocity"), 4, 0)
            planetsGrid.add(Label("Second Space Velocity"), 5, 0)

            SolarSystemPlanets.values().forEachIndexed { index, planet ->
                planetsGrid.add(Label(planet.name), 0, index + 1)
                planetsGrid.add(Label("%.2e".format(planet.planet.gravitationalAcceleration)), 1, index + 1)
                planetsGrid.add(Label("%.2f".format(planet.planet.radius)), 2, index + 1)
                planetsGrid.add(Label("%.2e".format(planet.planet.mass)), 3, index + 1)
                planetsGrid.add(Label("%.2e".format(planet.planet.firstSpaceVelocity)), 4, index + 1)
                planetsGrid.add(Label("%.2e".format(planet.planet.secondSpaceVelocity)), 5, index + 1)
            }

            // Добавляем GridPane на вкладку
            content = planetsGrid
        }

        val calculationsTab = Tab("Calculations")

        tabPane.tabs.addAll(planetsTab, calculationsTab)

        startCalculate(calculationsTab)

        val scene = Scene(tabPane, 700.0, 500.0)
        primaryStage.scene = scene
        primaryStage.show()
    }

    fun startCalculate(tab: Tab) {

        val grid = GridPane().apply {
            alignment = Pos.CENTER
            hgap = 10.0
            vgap = 10.0
            padding = Insets(25.0, 25.0, 25.0, 25.0)
        }

        grid.add(Label("Planet:"), 0, 0)
        planetComboBox = ComboBox<String>().apply {
            items.addAll(SolarSystemPlanets.values().map { it.name })
            selectionModel.select(0)
        }
        grid.add(planetComboBox, 1, 0)

        grid.add(Label("Mass (kg) on Earth:"), 0, 1)
        massTextField = TextField()
        grid.add(massTextField, 1, 1)

        val calculateButton = Button("Calculate weight")
        calculateButton.setOnAction { calculateWeight() }
        grid.add(calculateButton, 1, 2)

        weightLabel = Label()
        grid.add(weightLabel, 1, 3)

        tab.content = VBox(grid)
    }

    private fun calculateWeight() {
        val selectedPlanet = SolarSystemPlanets.valueOf(planetComboBox.selectionModel.selectedItem.uppercase())
        val massObject = massTextField.text.toDoubleOrNull()
        if (massObject == null) {
            weightLabel.text = "Invalid mass input"
            return
        }
        if (massObject < 0) {
            weightLabel.text = "The weight cannot be negative"
            return
        }
        val weight = weightCalculation.calculateWeight(selectedPlanet.planet, massObject)
        weightLabel.text = "Weight: %.2fkg".format(weight)
    }
}

fun main() {
    Application.launch(WeightCalculatorGUI::class.java)
}

main()
