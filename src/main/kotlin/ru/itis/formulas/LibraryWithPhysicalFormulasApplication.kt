package ru.itis.formulas

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class LibraryWithPhysicalFormulasApplication

fun main(args: Array<String>) {
    runApplication<LibraryWithPhysicalFormulasApplication>(*args)
}
